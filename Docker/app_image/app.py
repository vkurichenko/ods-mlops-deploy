"""
Inference app.
"""

import os
from typing import Dict

import mlflow
import pandas as pd
from fastapi import FastAPI, Body, File, UploadFile

inv_target_mapping = {0: "low", 1: "medium", 2: "high"}

app = FastAPI()


@app.get("/")
async def home() -> Dict:
    """
    Get info about the current model.

    Returns
    -------
    Dict
        Dictionary with info about the current model.
    """

    return {
        "health_check": "OK",
        "model_name": os.getenv("MLFLOW_MODEL_NAME"),
        "model_stage": os.getenv("MLFLOW_MODEL_STAGE"),
    }


@app.post("/predict_json")
async def predict_json(json_input: dict = Body()) -> Dict:
    """
    Get prediction from JSON input.

    Parameters
    ----------
    json_input : dict, optional
        Input JSON string, by default Body().

    Returns
    -------
    Dict
        Dictionary with prediction for input.
    """

    data = pd.json_normalize(json_input)
    model = mlflow.pyfunc.load_model(
        model_uri=f"models:/{os.getenv('MLFLOW_MODEL_NAME')}/{os.getenv('MLFLOW_MODEL_STAGE')}"
    )
    prediction = model.predict(data)
    return {"prediction": inv_target_mapping[int(prediction)]}


@app.post("/predict_csv")
async def predict_csv(file: UploadFile = File()) -> Dict:
    """
    Get prediction from '.csv' input.

    Parameters
    ----------
    file : UploadFile, optional
        Input '.csv' file, by default File().

    Returns
    -------
    Dict
        Dictionary with prediction for input.
    """

    data = pd.read_csv(file.file)
    model = mlflow.pyfunc.load_model(
        model_uri=f"models:/{os.getenv('MLFLOW_MODEL_NAME')}/{os.getenv('MLFLOW_MODEL_STAGE')}"
    )
    prediction = model.predict(data)
    return {"prediction": list(map(inv_target_mapping.get, prediction))}
